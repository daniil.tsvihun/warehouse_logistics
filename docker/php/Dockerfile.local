FROM php:8.1.0-fpm-alpine

# Set version of components
ENV VERSION_INSTALL_PHP_EXTENSIONS=1.4.18
ENV VERSION_COMPOSER=2.1.9
ENV VERSION_GNU_LIBICONV=1.15-r2

# ENV for install-php-extensions app for better compression of extensions
ENV IPE_LZF_BETTERCOMPRESSION=1

# Fix for preloadable_libiconv
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so

ADD ./docker/php/php.ini /usr/local/etc/php

# Install AWS Cli
RUN apk add --no-cache \
    python3 \
    py3-pip \
    busybox-suid \
    openssh \
    gnu-libiconv \
    jq \
    yarn \
    nano \
    supervisor \
    && pip3 install --upgrade pip \
    && pip3 install awscli

RUN wget https://github.com/mlocati/docker-php-extension-installer/releases/download/${VERSION_INSTALL_PHP_EXTENSIONS}/install-php-extensions -O /usr/local/bin/install-php-extensions \
    && chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions \
    curl \
    soap \
    exif \
    gd \
    iconv \
    mbstring \
    pdo \
    pdo_pgsql \
    pcntl \
    tokenizer \
    xml \
    zip \
    intl \
    bcmath \
    imap \
    xdebug \
    @composer-${VERSION_COMPOSER}

ADD . /var/www
RUN chown -R www-data:www-data /var/www

ADD ./docker/php/supervisord.conf /etc/supervisord.conf

# Enable status page
RUN echo "pm.status_path = /status" >> /usr/local/etc/php-fpm.conf

## Install and enable Xdebug
RUN echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.discover_client_host=false" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

WORKDIR /var/www
