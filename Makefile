build:
	@docker-compose up -d --build
	@docker-compose exec php sh -c "export APP_ENV=prod && composer install --no-dev --verbose --no-progress --no-interaction --prefer-dist --optimize-autoloader"
	@docker-compose exec php sh -c "composer dump-env prod"
	@docker-compose exec php sh -c "./bin/console cache:warmup --no-debug"
	@docker-compose exec php sh -c "./bin/console doctrine:migrations:migrate --no-debug --no-interaction"
	@open http://localhost:22002/api
	@docker-compose down