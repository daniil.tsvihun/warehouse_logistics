<?php

namespace App\Controller;

use App\Service\TruckServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class TruckController extends AbstractController
{
    private TruckServiceInterface $truckService;

    public function __construct(TruckServiceInterface $truckService)
    {
        $this->truckService = $truckService;
    }

    public function deleteAll(): Response
    {
        $this->truckService->deleteAll();
        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
