<?php

namespace App\Controller;

use App\Service\ProductServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ProductsController extends AbstractController
{
    private ProductServiceInterface $productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->productService = $productService;
    }

    public function createRandomProducts(): Response
    {
        $this->productService->createRandomProducts();
        return new Response(null, Response::HTTP_CREATED);
    }

    public function deleteAll(): Response
    {
        $this->productService->deleteAll();
        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
