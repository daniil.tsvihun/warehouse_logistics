<?php

namespace App\Controller;

use App\Entity\Truck;
use App\Service\ShipmentServiceInterface;
use App\Service\TruckServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Serializer;

class ShipmentController extends AbstractController
{
    private ShipmentServiceInterface $shipmentService;
    private TruckServiceInterface $truckService;
    private Serializer $serializer;

    public function __construct(
        ShipmentServiceInterface $shipmentService,
        TruckServiceInterface    $truckService,
    )
    {
        $this->shipmentService = $shipmentService;
        $this->truckService = $truckService;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        try {
            $truckMaxLoad = json_decode($request->getContent(), true)['truck_max_load'];
        } catch (\Throwable) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Bad request!');
        }
        $truck = (new Truck())->setMaxLoad($truckMaxLoad);
        $this->truckService->save($truck);

        $shipment = $this->shipmentService->create($truck);
        return new Response(
            $this->container->get('serializer')->serialize($shipment, 'json'),
            Response::HTTP_CREATED
        );
    }

    public function deleteAll(): Response
    {
        $this->shipmentService->deleteAll();
        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
