<?php

declare(strict_types=1);

namespace App\Dto;

final class TransportationInfoDto
{
    public function __construct(
        public readonly float $cost,
        public readonly float $weight,
    )
    {
    }
}