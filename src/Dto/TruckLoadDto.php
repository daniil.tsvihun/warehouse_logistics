<?php

declare(strict_types=1);

namespace App\Dto;

use App\Entity\Truck;
use Doctrine\Common\Collections\ArrayCollection;

final class TruckLoadDto
{
    public function __construct(
        public readonly Truck           $truck,
        public readonly float           $cost,
        public readonly float           $weightLoaded,
        public readonly array $loadProducts
    )
    {
    }
}