<?php

namespace App\Service;

use App\Entity\Shipment;
use App\Entity\Truck;

interface ShipmentServiceInterface
{
    /**
     * @param Truck $truck
     * @return Shipment
     */
    public function create(Truck $truck): Shipment;

    /**
     * @return void
     */
    public function deleteAll(): void;
}