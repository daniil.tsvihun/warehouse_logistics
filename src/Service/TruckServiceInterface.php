<?php

namespace App\Service;

use App\Entity\Truck;

interface TruckServiceInterface
{
    /**
     * @param Truck $truck
     * @return Truck
     */
    public function save(Truck $truck): Truck;

    public function deleteAll(): void;
}