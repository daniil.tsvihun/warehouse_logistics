<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\TransportationInfoDto;
use App\Dto\TruckLoadDto;
use App\Entity\Product;
use App\Entity\Truck;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LoadService implements LoadServiceInterface
{
    protected ProductServiceInterface $productService;

    public function __construct(ProductServiceInterface $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param Truck $truck
     * @return TruckLoadDto
     */
    public function load(Truck $truck): TruckLoadDto
    {
        $products = $this->productService->findAllActive();
        $productsToLoad = $this->getProductsToLoad($products, $truck->getMaxLoad());
        if (count($productsToLoad) === 0) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'There are no products that can be loaded to this truck.');
        }
        $transportationInfo = $this->getProductsTransportationInfo($productsToLoad);
        return new TruckLoadDto(
            truck: $truck,
            cost: $transportationInfo->cost,
            weightLoaded: $transportationInfo->weight,
            loadProducts: $productsToLoad
        );
    }

    /**
     * @param Product[] $products
     * @return TransportationInfoDto
     */
    protected function getProductsTransportationInfo(array $products): TransportationInfoDto
    {
        $totalCost = 60;
        $totalLoad = 0;
        foreach ($products as $product) {
            $totalLoad += $product->getWeight();
            $totalCost += $this->getLoadCost($product);
        }

        return new TransportationInfoDto(
            cost: $totalCost,
            weight: $totalLoad
        );
    }

    /**
     * @param Product[] $products
     * @param float $capacity
     * @return array
     */
    protected function getProductsToLoad(array $products, float $capacity): array
    {
        $itemsCount = count($products);
        $comparingData = [];

        for ($i = 0; $i <= $capacity; $i++) {
            $comparingData[0][$i] = [
                'value' => 0,
                'subset' => [],
            ];
        }
        for ($j = 0; $j <= $itemsCount; $j++) {
            $comparingData[$j][0] = [
                'value' => 0,
                'subset' => [],
            ];
        }

        $productValues = [];

        for ($j = 1; $j <= $itemsCount; $j++) {
            for ($i = 1; $i <= $capacity; $i++) {
                $comparingData[$j][$i] = $comparingData[$j - 1][$i];
                $currentProduct = $products[$j - 1];
                // Exclude products with weight bigger than maximum capacity
                if (
                    $currentProduct->getWeight() > $capacity
                    || !isset($comparingData[$j - 1][$i - $currentProduct->getWeight()])
                ) {
                    continue;
                }

                if (isset($productValues[$j - 1])) {
                    $currentProductValue = $productValues[$j - 1];
                } else {
                    $productValues[$j - 1] = $currentProductValue = $this->getLoadValue($currentProduct);
                }

                $previousMax = $comparingData[$j - 1][$i - $currentProduct->getWeight()];
                $tmp = $previousMax['value'] + $currentProductValue;
                if ($tmp > $comparingData[$j][$i]['value']) {
                    $comparingData[$j][$i] = [
                        'value' => $tmp,
                        'subset' => array_merge($previousMax['subset'], [$products[$j - 1]]),
                    ];
                }
            }
        }

        unset($productValues);
        return $comparingData[$itemsCount][$capacity]['subset'];
    }

    /**
     * @param Product $product
     * @return float
     */
    private function getLoadCost(Product $product): float
    {
        return $product->getWeight() / 50 + 1;
    }

    /**
     * Cost per weight. The bigger the value the more efficient cost of loading
     *
     * @param Product $product
     * @return float
     */
    private function getLoadValue(Product $product): float
    {
        return $this->getLoadCost($product) / $product->getWeight();
    }
}