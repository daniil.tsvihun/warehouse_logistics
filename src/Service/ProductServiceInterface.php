<?php

namespace App\Service;

interface ProductServiceInterface
{
    public function findAllActive(): array;

    /**
     * @param int $stockLoad Stock of product (kg)
     * @param int $weightMin Min product weight (kg)
     * @param int $weightMax Max product weight (kg)
     * @return array
     */
    public function createRandomProducts(
        int $stockLoad = 100000,
        int $weightMin = 55,
        int $weightMax = 5555
    ): array;

    /**
     * @return void
     */
    public function deleteAll(): void;
}