<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Shipment;
use App\Entity\Truck;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

class ShipmentService extends EntityService implements ShipmentServiceInterface
{
    private LoadServiceInterface $loadService;

    public function __construct(ManagerRegistry $doctrine, LoadServiceInterface $loadService)
    {
        parent::__construct($doctrine);
        $this->loadService = $loadService;
    }

    /**
     * @param Truck $truck
     * @return Shipment
     */
    public function create(Truck $truck): Shipment
    {
        $loadDto = $this->loadService->load($truck);
        $shipment = (new Shipment())
            ->setCost($loadDto->cost)
            ->setWeightLoaded($loadDto->weightLoaded)
            ->addProducts(new ArrayCollection($loadDto->loadProducts))
            ->setTruck($truck);
        $this->entityManager->persist($shipment);
        $this->entityManager->flush();
        return $shipment;
    }

    public function deleteAll(): void
    {
        $this->entityManager->getRepository(Shipment::class)->removeAll(true);
    }
}