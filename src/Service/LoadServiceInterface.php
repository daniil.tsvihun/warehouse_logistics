<?php

namespace App\Service;

use App\Dto\TruckLoadDto;
use App\Entity\Truck;

interface LoadServiceInterface
{
    public function load(Truck $truck): TruckLoadDto;
}