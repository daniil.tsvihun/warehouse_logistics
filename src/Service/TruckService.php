<?php

declare(strict_types=1);

namespace App\Service;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use App\Entity\Truck;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TruckService extends EntityService implements TruckServiceInterface
{
    private ValidatorInterface $validator;

    public function __construct(ManagerRegistry $doctrine, ValidatorInterface $validator)
    {
        parent::__construct($doctrine);
        $this->validator = $validator;
    }

    /**
     * @param Truck $truck
     * @return Truck
     */
    public function save(Truck $truck): Truck
    {
        $errors = $this->validator->validate($truck);
        if ($errors->count() > 0) {
            throw new ValidationException($errors, 'Truck validation failed.');
        }
        $this->entityManager->persist($truck);
        $this->entityManager->flush();
        return $truck;
    }

    public function deleteAll(): void
    {
        $this->entityManager->getRepository(Truck::class)->removeAll(true);
    }
}