<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;

class ProductService extends EntityService implements ProductServiceInterface
{

    /**
     * @param int $stockLoad Stock of product (kg)
     * @param int $weightMin Min product weight (kg)
     * @param int $weightMax Max product weight (kg)
     * @return array
     */
    public function createRandomProducts(
        int $stockLoad = 100000,
        int $weightMin = 55,
        int $weightMax = 5555
    ): array {
        /** @var Product[] $products */
        $products = [];
        $productsSum = 0;

        while ($productsSum < $stockLoad) {
            $product = new Product();
            $weight = mt_rand($weightMin, $weightMax);
            $productsSum += $weight;
            $product->setWeight($weight);
            $this->entityManager->persist($product);
            $products[] = $product;
        }

        /** Check if we need to adjust stock products weights to be equal exactly $stockLoad */
        if ($productsSum > $stockLoad) {
            $diff = $productsSum - $stockLoad;
            foreach ($products as $product) {
                if (($product->getWeight() - $diff) >= $weightMin) {
                    $product->setWeight($product->getWeight() - $diff);
                    $this->entityManager->persist($product);
                    break;
                }
            }
        }

        $this->entityManager->flush();
        return $products;
    }

    public function deleteAll(): void
    {
        $this->entityManager->getRepository(Product::class)->removeAll(true);
    }

    /**
     * @return array
     */
    public function findAllActive(): array
    {
        return $this->entityManager
            ->getRepository(Product::class)
            ->findBy(['shipment' => null]);
    }
}