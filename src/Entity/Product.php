<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ProductsController;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ApiResource(
    collectionOperations: [
        'delete_all' => [
            'method' => 'delete',
            'path' => '/products',
            'controller' => [ProductsController::class, 'deleteAll'],
            'openapi_context' => [
                'summary' => 'Deletes all Products.',
            ]
        ],
        'create_random' => [
            'method' => 'post',
            'path' => '/products',
            'controller' => [ProductsController::class, 'createRandomProducts'],
            'openapi_context' => [
                'summary' => 'Creates random products.',
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [],
                            ]
                        ]
                    ]
                ],
                'responses' => [
                    201 => [
                        'description' => 'Random products created',
                        'content' => []
                    ],
                ]
            ]
        ],
        'get'
    ],
    itemOperations: [
        'get'
    ]
)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float')]
    private $weight;

    #[ORM\ManyToOne(targetEntity: Shipment::class, cascade: ['persist', 'remove'], inversedBy: 'products')]
    #[ORM\JoinColumn(onDelete: "CASCADE")]
    private $shipment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getShipment(): ?Shipment
    {
        return $this->shipment;
    }

    public function setShipment(?Shipment $shipment): self
    {
        $this->shipment = $shipment;

        return $this;
    }
}
