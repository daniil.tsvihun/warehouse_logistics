<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\TruckController;
use App\Repository\TruckRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

#[ORM\Entity(repositoryClass: TruckRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'delete_all' => [
            'method' => 'delete',
            'path' => '/trucks',
            'controller' => [TruckController::class, 'deleteAll'],
            'openapi_context' => [
                'summary' => 'Deletes all Trucks and linked Shipments.',
            ]
        ],
    ],
    itemOperations: [
        'get',
    ]
)]
class Truck
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float')]
    private $maxLoad;

    #[ORM\OneToOne(mappedBy: 'truck', targetEntity: Shipment::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(onDelete: "CASCADE")]
    private $shipment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaxLoad(): ?float
    {
        return $this->maxLoad;
    }

    public function setMaxLoad(float $maxLoad): self
    {
        $this->maxLoad = $maxLoad;

        return $this;
    }

    public function getShipment(): ?Shipment
    {
        return $this->shipment;
    }

    public function setShipment(Shipment $shipment): self
    {
        // set the owning side of the relation if necessary
        if ($shipment->getTruck() !== $this) {
            $shipment->setTruck($this);
        }

        $this->shipment = $shipment;

        return $this;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('maxLoad', new NotBlank());
        $metadata->addPropertyConstraint('maxLoad', new GreaterThanOrEqual(1000));
        $metadata->addPropertyConstraint('maxLoad', new LessThanOrEqual(10000));
    }
}
