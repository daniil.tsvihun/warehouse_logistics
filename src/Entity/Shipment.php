<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ShipmentController;
use App\Dto\ShipmentRequestDto;
use App\Repository\ShipmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [
        'get',
        'delete_all' => [
            'method' => 'delete',
            'path' => '/shipments',
            'controller' => [ShipmentController::class, 'deleteAll'],
            'openapi_context' => [
                'summary' => 'Deletes all products.',
            ]
        ],
        'create_shipment' => [
            'method' => 'post',
            'path' => '/shipments',
            'controller' => [ShipmentController::class, 'create'],
            'openapi_context' => [
                'summary' => 'Creates Truck with maxLoad and loads it with Products',
                'description' => 'Creates new Truck with specified maxLoad and efficiently loads it with Products.',
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'truck_max_load' => [
                                        'default' => 1000,
                                        'type' => 'integer',
                                        'description' => 'Max load of truck that will be used',
                                        'nullable' => true,
                                    ]
                                ],
                            ],
                            'example' => [
                                'truck_max_load' => 1000,
                            ]
                        ]
                    ]
                ],
                'responses' => [
                    200 => [
                        'description' => 'Truck loaded and Shipment created',
                        'content' => []
                    ],
                ]
            ]
        ],
    ],
    itemOperations: [
        'get',
        'delete',
    ]
)]
#[ORM\Entity(repositoryClass: ShipmentRepository::class)]
class Shipment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToOne(inversedBy: 'shipment', targetEntity: Truck::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    private $truck;

    #[ORM\OneToMany(mappedBy: 'shipment', targetEntity: Product::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    private $products;

    #[ORM\Column(type: 'float')]
    private $cost;

    #[ORM\Column(type: 'float')]
    private $weightLoaded;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTruckMaxLoad(): int
    {
        return $this->getTruck()?->getMaxLoad();
    }

    public function getTruck(): ?Truck
    {
        return $this->truck;
    }

    public function setTruck(Truck $truck): self
    {
        $this->truck = $truck;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setShipment($this);
        }

        return $this;
    }

    /**
     * @param Collection<int, Product> $products
     * @return $this
     */
    public function addProducts(Collection $products): self
    {
        foreach ($products as $product) {
            $this->addProduct($product);
        }
        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getShipment() === $this) {
                $product->setShipment(null);
            }
        }

        return $this;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getWeightLoaded(): ?float
    {
        return $this->weightLoaded;
    }

    public function setWeightLoaded(float $weightLoaded): self
    {
        $this->weightLoaded = $weightLoaded;

        return $this;
    }
}
