# Warehouse Logistics

The program efficiently loads random products to the truck with specified capacity and creates Shipment object.

Efficiently means that cost-efficient.

The value of the product in that way can be calculated like: `product_load_cost / product_weight`.

The program picks the best total value for given truck capacity.

## Requirements
- [ ] [docker](https://docs.docker.com/get-docker/)
- [ ] [docker-compose](https://docs.docker.com/compose/install/)

## Install
### Clone project

```
$ git clone https://gitlab.com/daniil.tsvihun/warehouse_logistics.git
```

### Deploy and start using


`$ cd warehouse_logistics`

`$ make`

After the API documentation will be opened in you browser.

![img.png](img.png)

If it is not happened, then just open http://localhost:22002/api
***

## Workflow

### Create random products

Run `POST /api/products` with no parameters
HTTP 201 with empty response body means that products created.

### Get products
Run `GET /api/products`

### Load products to truck
Run `POST /api/shipments` with specification of the truck capacity (in kg). Min: 1000, max: 10000.

In response, you will get Shipment object if successful.
Example:
```
{
  "id": 1,
  "truck": "/api/trucks/1",
  "products": [
    "/api/products/8",
    "/api/products/10"
  ],
  "cost": 78.94,
  "weightLoaded": 847,
  "truckMaxLoad": 1000
}
```
| Field        | Description                                            |
|--------------|--------------------------------------------------------|
| id           | ID of the Shipment object.                             |
| truck        | `/api/trucks/{id}` where `{id}` is ID of created Truck |
| products     | Array of loaded Products                               |
| cost         | Cost of loaded Products                                |
| weightLoaded | Total weight of loaded products                        |
| truckMaxLoad | The total capacity of the truck.                       |

## List all created Shipments

Run `GET /api/shipments`

## Remove all Products
Run `DELETE /api/products`

## Remove all Shipments
Run `DELETE /api/shipments`
